#### Google Compute Engine Documentation

Cloud/[GoogleComputeEngine](https://wiki.debian.org/GoogleComputeEngine) Image lists for Google Compute Engine

Google Compute Engine documentation: [https://cloud.google.com/compute/docs/](https://cloud.google.com/compute/docs/)

Google Cloud SDK (gcloud CLI) Documentation [https://cloud.google.com/sdk/](https://cloud.google.com/sdk/)

#### Debian Images

Google Compute Engine Debian images are built with bootstrap-vz. All the configuration and manifest data is provided open source as part of that project [https://github.com/andsens/bootstrap-vz/tree/master/manifests/official/gce](https://github.com/andsens/bootstrap-vz/tree/master/manifests/official/gce).

Debian community members are welcome to help improve and maintain the images in Google Compute Engine. Issues can be filed in the compute-image-packages Github project [https://github.com/GoogleCloudPlatform/compute-image-packages/issues](https://github.com/GoogleCloudPlatform/compute-image-packages/issues).

The images deviate from official Debian images in order to make Debian work correctly on the platform and provide a baseline user experience. Details about what is configured or added is maintained in the Google Compute Engine Images documentation [https://cloud.google.com/compute/docs/images](https://cloud.google.com/compute/docs/images).

For discussion about Debian on various cloud providers, please visit the debian-cloud mailing list.

#### SSH user accounts

SSH in Google Compute Engine is provided using user provided SSH keys. There is not a default account or password configured in GCE images. For more information on how to SSH into GCE Debian instances, refer to the Google Compute Engine SSH documentation: [https://cloud.google.com/compute/docs/instances/connecting-to-instance](https://cloud.google.com/compute/docs/instances/connecting-to-instance)

The software to facilitate SSH key based login via metadata is provided in the base images and is open source licensed under the Apache License 2.0 [https://github.com/GoogleCloudPlatform/compute-image-packages](https://github.com/GoogleCloudPlatform/compute-image-packages).

#### Working with Future Images

Future images may be available from the separate GCP project called debian-cloud-testing. To list and use future images (like Debian Stretch before it was released) please use the following:

```
gcloud compute images list --project debian-cloud-testing --no-standard-images
gcloud compute instances create my-test-machine --image-project debian-cloud-testing --image-family debian-9
```

#### Listing Current Images

To list the currently-recommended, newest versions of Debian images on Google Compute Engine, use the Google Cloud SDK command line tool gcloud https://cloud.google.com/compute/docs/gcloud-compute/:

```
gcloud compute images list --project=debian-cloud --no-standard-images
```

#### Using An Image

To use the most up to date Debian 9 Stretch image, use the "debian-9" image family when creating a new instance.

```
gcloud compute instances create "<instance-name>" --image-project debian-cloud --image-family debian-9 --zone="<zone>"
```

#### Listing Older and Deprecated Images

As Google Compute Engine releases new images, older images will be deprecated and eventually removed. When this happens, Google Compute Engine sets the deprecation status on an image.

```
gcloud compute images list --project=debian-cloud --no-standard-images --show-deprecated
```

#### Building a Google Compute Engine Image
To build a Debian image for Google Compute Engine, follow the guide for building custom images in Google Compute Engine: [https://cloud.google.com/compute/docs/tutorials/building-images](https://cloud.google.com/compute/docs/tutorials/building-images)

You can also create your own bootstrap-vz manifest and build an image using the baseline manifests as an example: [https://github.com/andsens/bootstrap-vz/tree/master/manifests/official/gce](https://github.com/andsens/bootstrap-vz/tree/master/manifests/official/gce)

#### Google Contacts

If you have any questions, concerns, or general feedback, please don’t hesitate to contact the Google Compute team ([gc-team@google.com](gc-team@google.com)) or open an issue in the compute-image-packages Github project [https://github.com/GoogleCloudPlatform/compute-image-packages/issues](https://github.com/GoogleCloudPlatform/compute-image-packages/issues.).

#### Google Compute Engine Debian Experiments Project

A project, "debian-cloud-experiments", for Debian is available with limited quota for Debian Developers to experiment with the platform. Please let us know if you would like access to experiment in this project.