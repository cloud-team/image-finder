"""Initial database

Revision ID: 6d7bb3b7952e
Revises:
Create Date: 2022-02-09 23:06:46.345999

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6d7bb3b7952e'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'packages',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('name', sa.String(length=80), nullable=False),
        sa.Column('version', sa.String(length=80), nullable=False),
        sa.Column('tracker', sa.String(length=150), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_table(
        'providers',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('name', sa.String(length=80), nullable=False),
        sa.Column('vendor', sa.String(length=80), nullable=False),
        sa.Column('description', sa.Text(), nullable=True),
        sa.Column('markdown', sa.Text(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('name'),
        sa.UniqueConstraint('vendor')
    )
    op.create_table(
        'users',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('public_id', sa.String(length=255), nullable=True),
        sa.Column('name', sa.String(length=86), nullable=True),
        sa.Column('email', sa.String(length=86), nullable=True),
        sa.Column('username', sa.String(length=86), nullable=False),
        sa.Column('avatar_url', sa.String(length=255), nullable=True),
        sa.Column('admin', sa.Boolean(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('email'),
        sa.UniqueConstraint('public_id'),
        sa.UniqueConstraint('username')
    )
    op.create_table(
        'flask_dance_oauth',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('provider', sa.String(length=50), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('token', sa.JSON(), nullable=False),
        sa.Column('user_id', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_table(
        'images',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('uid', sa.String(length=255), nullable=True),
        sa.Column('arch', sa.String(length=255), nullable=False),
        sa.Column('release', sa.String(length=255), nullable=False),
        sa.Column('image_type', sa.String(length=255), nullable=False),
        sa.Column('vendor', sa.String(length=255), nullable=False),
        sa.Column('version', sa.String(length=255), nullable=False),
        sa.Column('region', sa.String(length=255), nullable=True),
        sa.Column('artifact_url', sa.String(length=255), nullable=True),
        sa.Column('ref', sa.String(length=255), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('provider_id', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['provider_id'], ['providers.id'], ),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('uid')
    )
    op.create_table(
        'service_tokens',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('name', sa.String(length=80), nullable=False),
        sa.Column('body', sa.String(length=255), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('expires_at', sa.DateTime(), nullable=False),
        sa.Column('user_id', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('body')
    )
    op.create_table(
        'images_packages',
        sa.Column('image_id', sa.Integer(), nullable=False),
        sa.Column('package_id', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['image_id'], ['images.id'], ),
        sa.ForeignKeyConstraint(['package_id'], ['packages.id'], ),
        sa.PrimaryKeyConstraint('image_id', 'package_id')
    )


def downgrade():
    op.drop_table('images_packages')
    op.drop_table('service_tokens')
    op.drop_table('images')
    op.drop_table('flask_dance_oauth')
    op.drop_table('users')
    op.drop_table('providers')
    op.drop_table('packages')
