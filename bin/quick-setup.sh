#!/bin/sh

set -e


lint() {
    python3 -m flake8
}

lint_chart() {
    helm plugin install https://github.com/datreeio/helm-datree
    helm datree config set offline local
    helm datree test charts/debian-cloud-image-finder
}

test() {
    python3 -m coverage run --source='.' -m nose2
}

coverage() {
    python3 -m coverage report -m
}

deploy_staging() {
    python3 deploy/staging.py
}

deploy_production() {
    python3 deploy/production.py
}

install_packages() {
    SUDO="sudo"
    if [ $(whoami) = "root" ]; then
        SUDO=""
    fi

    APT_OPTIONS=""
    if [ "$NON_INTERACTIVE" = "1" ]; then
        APT_OPTIONS="--no-install-recommends -y "
    fi

    if [ "$DOCKER_ENVIRONMENT" = "1" ]; then
        $SUDO apt-get update && apt-get upgrade -y
    fi

    $SUDO apt-get install $APT_OPTIONS \
        gunicorn \
        python3-blinker \
        python3-coverage \
        python3-dev \
        python3-dotenv \
        python3-dynaconf \
        python3-flasgger \
        python3-flask \
        python3-flask-dance \
        python3-flask-login \
        python3-flask-marshmallow \
        python3-flask-migrate \
        python3-flask-restful \
        python3-flask-seeder \
        python3-flaskext.wtf \
        python3-flask-sqlalchemy \
        python3-flask-testing \
        python3-flake8 \
        python3-flake8-spellcheck \
        python3-jwt \
        python3-markdown2 \
        python3-marshmallow-sqlalchemy \
        python3-nose2 \
        python3-psycopg2 \
        python3-pymysql \
        python3-requests \
        python3-setuptools \
        python3-werkzeug \
        python3-yaml \
        libjs-bootstrap4 \
        libpq-dev \
        libjs-jquery \
        libjs-jquery-datatables \
        libjs-simplemde \
        fonts-font-awesome

    if [ "$DOCKER_ENVIRONMENT" = "1" ]; then
        apt-get -q -y clean && \
        rm -rf /var/lib/apt/lists/*
    fi
}


case "$1" in
    "")
        install_packages
        ;;
    install_packages)
        install_packages
        ;;
    lint)
        lint
        ;;
    lint_chart)
        lint_chart
        ;;
    test)
        test
	    ;;
    coverage)
        coverage
	    ;;
    deploy_staging)
        deploy_staging
        ;;
    deploy_production)
        deploy_production
        ;;
esac
