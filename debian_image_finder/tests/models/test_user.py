from debian_image_finder.tests.models.base import TestBase
from debian_image_finder.models.user import User


class TestUserModel(TestBase):

    def test_user_count(self):
        """
        Test number of records in User table.
        """
        self.assertEqual(User.query.count(), 1)

    def test_create_non_admin_user(self):
        """
        Test create nonadmin User.
        """
        non_admin_user = User(
            name='non-admin',
            username='non-admin',
            email='non-admin@email.com',
            avatar_url='',
            admin=False
        )
        non_admin_user.save_to_db()

        retrieved_user = User.find_by_id(non_admin_user.id)

        self.assertEqual(User.query.count(), 2)
        self.assertEqual(retrieved_user.id, non_admin_user.id)
        self.assertEqual(retrieved_user.public_id, non_admin_user.public_id)
        self.assertEqual(retrieved_user.name, non_admin_user.name)
        self.assertEqual(retrieved_user.username, non_admin_user.username)
        self.assertEqual(retrieved_user.avatar_url, non_admin_user.avatar_url)
        self.assertFalse(retrieved_user.admin)

    def test_create_admin_user(self):
        """
        Test create admin User.
        """
        admin_user = User(
            name='admin-2',
            username='admin-2',
            email='admin-2@email.com',
            avatar_url='',
            admin=True
        )
        admin_user.save_to_db()

        retrieved_user = User.find_by_id(admin_user.id)

        self.assertEqual(User.query.count(), 2)
        self.assertEqual(retrieved_user.id, admin_user.id)
        self.assertEqual(retrieved_user.public_id, admin_user.public_id)
        self.assertEqual(retrieved_user.name, admin_user.name)
        self.assertEqual(retrieved_user.username, admin_user.username)
        self.assertEqual(retrieved_user.avatar_url, admin_user.avatar_url)
        self.assertTrue(retrieved_user.admin)

    def test_find_user_by_id(self):
        """
        Test find user by id.
        """
        retrieved_user = User.find_by_id(self.admin_user.id)

        self.assertEqual(retrieved_user.id, self.admin_user.id)
        self.assertEqual(retrieved_user.public_id, self.admin_user.public_id)
        self.assertEqual(retrieved_user.name, self.admin_user.name)
        self.assertEqual(retrieved_user.username, self.admin_user.username)
        self.assertEqual(retrieved_user.avatar_url, self.admin_user.avatar_url)
        self.assertTrue(retrieved_user.admin)

    def test_find_user_by_public_id(self):
        """
        Test find user by public id.
        """
        retrieved_user = User.find_by_public_id(self.admin_user.public_id)

        self.assertEqual(retrieved_user.id, self.admin_user.id)
        self.assertEqual(retrieved_user.public_id, self.admin_user.public_id)
        self.assertEqual(retrieved_user.name, self.admin_user.name)
        self.assertEqual(retrieved_user.username, self.admin_user.username)
        self.assertEqual(retrieved_user.avatar_url, self.admin_user.avatar_url)
        self.assertTrue(retrieved_user.admin)

    def test_find_user_by_name(self):
        """
        Test find user by name.
        """
        retrieved_user = User.find_by_name(self.admin_user.name)

        self.assertEqual(retrieved_user.id, self.admin_user.id)
        self.assertEqual(retrieved_user.public_id, self.admin_user.public_id)
        self.assertEqual(retrieved_user.name, self.admin_user.name)
        self.assertEqual(retrieved_user.username, self.admin_user.username)
        self.assertEqual(retrieved_user.avatar_url, self.admin_user.avatar_url)
        self.assertTrue(retrieved_user.admin)

    def test_find_user_by_username(self):
        """
        Test find user by username.
        """
        retrieved_user = User.find_by_username(self.admin_user.username)

        self.assertEqual(retrieved_user.id, self.admin_user.id)
        self.assertEqual(retrieved_user.public_id, self.admin_user.public_id)
        self.assertEqual(retrieved_user.name, self.admin_user.name)
        self.assertEqual(retrieved_user.username, self.admin_user.username)
        self.assertEqual(retrieved_user.avatar_url, self.admin_user.avatar_url)
        self.assertTrue(retrieved_user.admin)

    def test_find_all_users(self):
        """
        Test find all users.
        """
        retrieved_users = User.find_all()
        self.assertEqual(len(retrieved_users), 1)

        admin_user = User(
            name='admin-2',
            username='admin-2',
            email='admin-2@email.com',
            avatar_url='',
            admin=True
        )
        admin_user.save_to_db()

        retrieved_users = User.find_all()
        self.assertEqual(len(retrieved_users), 2)

    def test_delete_user_from_db(self):
        """
        Test delete a user from db.
        """
        self.admin_user.delete_from_db()

        self.assertEqual(User.query.count(), 0)
