import unittest
from debian_image_finder.tests.config.base import TestBase


class TestConfig(TestBase):

    def test_config(self):
        self.assertTrue(self.app.config['SECRET_KEY'] == 'my_precious')
        self.assertFalse(self.app is None)
        self.assertTrue(self.app.config['DEBUG'])
        self.assertFalse(self.app.config['SQLALCHEMY_TRACK_MODIFICATIONS'])
        self.assertTrue(
            self.app.config['SQLALCHEMY_DATABASE_URI'] == 'sqlite:///development.db'
        )


if __name__ == '__main__':
    unittest.main()
