import json
import unittest
from debian_image_finder.tests.api.base import TestBase
from debian_image_finder.models.image import Image
from debian_image_finder.models.provider import Provider
from debian_image_finder.extensions.database import db


class TestImageApi(TestBase):
    """Tests for the Image API."""

    def test_post_image(self):

        provider = Provider(
            name='provider',
            vendor='vendor',
            description='test',
            markdown='markdown'
        )
        provider.save_to_db()

        args = {
            'arch': 'arch',
            'release': 'release',
            'image_type': 'image_type',
            'vendor': 'vendor',
            'version': 'version',
            'uid': 'uid',
            'artifact_url': 'artifact_url',
            'region': 'region',
            'ref': '/ref.tar.xz',
            'provider_id': provider.id
        }

        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.token}'
        }

        response = self.client.post(
            '/api/v1/image',
            data=json.dumps(args),
            headers=headers
        )

        data = json.loads(response.data.decode())

        self.assertEqual(response.status_code, 201)
        self.assertIn('arch', data['arch'])
        self.assertIn('release', data['release'])
        self.assertIn('image_type', data['image_type'])
        self.assertIn('vendor', data['vendor'])
        self.assertIn('version', data['version'])
        self.assertIn('region', data['region'])
        self.assertIn('ref', data['ref'])
        self.assertEqual(provider.id, data['provider_id'])

    def test_get_image(self):
        """Ensure the /image route behaves correctly."""

        provider = Provider(
            name='provider',
            vendor='vendor',
            description='test',
            markdown='markdown'
        )
        db.session.add(provider)
        db.session.commit()

        image = Image(
            arch='arch',
            release='release',
            image_type='image_type',
            vendor='vendor',
            version='version',
            uid='uid',
            artifact_url="artifact_url",
            region='region',
            ref='/ref.tar.xz',
            provider_id=provider.id
        )

        db.session.add(image)
        db.session.commit()

        response = self.client.get(
            f'/api/v1/image/{image.uid}'
        )

        data = json.loads(response.data.decode())

        self.assertEqual(response.status_code, 200)
        self.assertIn('arch', data['arch'])
        self.assertIn('release', data['release'])
        self.assertIn('image_type', data['image_type'])
        self.assertIn('vendor', data['vendor'])
        self.assertIn('version', data['version'])
        self.assertIn('region', data['region'])
        self.assertIn('ref', data['ref'])

    def test_put_image(self):
        """Ensure the /image route behaves correctly."""

        provider = Provider(
            name='provider',
            vendor='vendor',
            description='test',
            markdown='markdown'
        )
        db.session.add(provider)
        db.session.commit()

        image = Image(
            arch='arch',
            release='release',
            image_type='image_type',
            vendor='vendor',
            version='version',
            uid='uid',
            artifact_url="artifact_url",
            region='region',
            ref='/ref.tar.xz',
            provider_id=provider.id
        )

        db.session.add(image)
        db.session.commit()

        args = {
            'arch': 'arch_change',
            'release': 'release_change',
            'image_type': 'image_type_change',
            'vendor': 'vendor_change',
            'version': 'version_change',
            'uid': 'uid_change',
            'artifact_url': 'artifact_url_change',
            'region': 'region_change',
            'ref': '/ref_change.tar.xz',
            'provider_id': provider.id
        }

        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.token}'
        }

        response = self.client.put(
            f'/api/v1/image/{image.uid}',
            data=json.dumps(args),
            headers=headers
        )

        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('arch_change', data['arch'])
        self.assertIn('release_change', data['release'])
        self.assertIn('image_type_change', data['image_type'])
        self.assertIn('vendor_change', data['vendor'])
        self.assertIn('version_change', data['version'])
        self.assertIn('region_change', data['region'])
        self.assertIn('ref_change', data['ref'])
        self.assertEqual(provider.id, data['provider_id'])

    def test_delete_image(self):
        """Ensure the /images route behaves correctly."""

        provider = Provider(
            name='provider',
            vendor='vendor',
            description='test',
            markdown='markdown'
        )
        db.session.add(provider)
        db.session.commit()

        image = Image(
            arch='arch',
            release='release',
            image_type='image_type',
            vendor='vendor',
            version='version',
            uid='uid',
            artifact_url="artifact_url",
            region='region',
            ref='/ref.tar.xz',
            provider_id=provider.id
        )

        db.session.add(image)
        db.session.commit()

        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.token}'
        }

        response = self.client.delete(
            f'/api/v1/image/{image.uid}',
            headers=headers
        )

        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('Image deleted successfully.', data['message'])


if __name__ == '__main__':
    unittest.main()
