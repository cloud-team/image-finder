import json
from debian_image_finder.tests.api.base import TestBase
from debian_image_finder.models.provider import Provider
from debian_image_finder.extensions.database import db


class TestPublishApi(TestBase):
    """Tests for the Publish API."""

    def test_publish(self):

        provider = Provider(
            name='provider',
            vendor='vendor',
            description='test',
            markdown='markdown'
        )
        db.session.add(provider)
        db.session.commit()

        args = {
            'items': [
                {
                    'data': {
                        'provider': 'cloud.debian.org',
                        'ref': 'ref.tar.xz'
                    },
                    'kind': 'Upload',
                    'metadata': {
                        'labels': {
                            'cloud.debian.org/vendor': 'vendor',
                            'cloud.debian.org/version': 'version',
                            'debian.org/arch': 'arch',
                            'debian.org/release': 'release',
                            'upload.cloud.debian.org/image-format': 'internal',
                            'upload.cloud.debian.org/type': 'release'
                        },
                        'uid': 'uid'
                    }
                }
            ]
        }

        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.token}'
        }

        response = self.client.post(
            '/api/v1/publish',
            data=json.dumps(args),
            headers=headers
        )

        data = json.loads(response.data.decode())
        image = data['images'][0]
        self.assertEqual(response.status_code, 201)
        self.assertIn('arch', image['arch'])
        self.assertIn(
            'http://cloud.debian.org/cdimage/cloud/ref.tar.xz',
            image['artifact_url']
        )
        self.assertIn('release', image['image_type'])
        self.assertIn('ref.tar.xz', image['ref'])
        self.assertIn('', image['region'])
        self.assertIn('arch', image['arch'])
        self.assertIn('release', image['release'])
        self.assertIn('uid', image['uid'])
        self.assertIn('vendor', image['vendor'])
        self.assertIn('version', image['version'])
