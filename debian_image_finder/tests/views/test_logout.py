import unittest
from flask import url_for
from debian_image_finder.tests.views.base import TestBase


class TestLogoutView(TestBase):

    def test_logout_view(self):
        """
        Test that logout page redirects to index page.
        """
        response = self.client.get(
            url_for('.logout',)
        )

        self.assertEqual(response.status_code, 302)


if __name__ == '__main__':
    unittest.main()
