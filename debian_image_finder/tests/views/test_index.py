import unittest
from flask import url_for
from debian_image_finder.tests.views.base import TestBase


class TestIndexView(TestBase):

    def test_index_view(self):
        """
        Test that index is accessible without login
        """
        response = self.client.get(url_for('.index'))

        self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    unittest.main()
