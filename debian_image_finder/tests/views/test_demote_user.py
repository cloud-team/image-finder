import unittest
from flask import url_for
from debian_image_finder.tests.views.base import TestBase


class TestDemoteUserView(TestBase):

    def test_demote_user_view(self):
        """
        Test that demote user page is inaccessible without login
        and redirects to index page.
        """
        response = self.client.get(
            url_for('.demote_user', public_id=self.user.public_id)
        )
        redirect_url = url_for('.index')

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, redirect_url)


if __name__ == '__main__':
    unittest.main()
