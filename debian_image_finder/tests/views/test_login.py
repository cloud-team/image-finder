import unittest
from flask import url_for
from debian_image_finder.tests.views.base import TestBase


class TestLoginView(TestBase):

    def test_login_view(self):
        """
        Test that login page is accessible without login.
        """
        response = self.client.get(
            url_for('.login',)
        )

        self.assertEqual(response.status_code, 302)


if __name__ == '__main__':
    unittest.main()
