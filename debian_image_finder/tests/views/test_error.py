import unittest
from flask import abort, Blueprint
from debian_image_finder.tests.views.base import TestBase


class TestErrorPages(TestBase):

    def test_403_forbidden(self):
        """
        Create route to abort the request with the 403 Forbidden.
        """
        def forbidden_error():
            abort(403)

        bp = Blueprint('403', __name__, template_folder='templates')
        bp.add_url_rule('/403', view_func=forbidden_error)
        self.app.register_blueprint(bp, name='403')

        response = self.client.get('/403')

        self.assertEqual(response.status_code, 403)
        self.assertTrue("403 Forbidden" in str(response.data))

    def test_404_not_found(self):
        """
        Requests a route that does not exist, showing 500 Internal Server Error.
        """
        response = self.client.get('/nothinghere')

        self.assertEqual(response.status_code, 404)
        self.assertTrue("404 Not Found" in str(response.data))

    def test_500_internal_server_error(self):
        """
        Create route to abort the request with the 500 Internal Server Error.
        """
        def internal_server_error():
            abort(500)

        bp = Blueprint('500', __name__, template_folder='templates')
        bp.add_url_rule('/500', view_func=internal_server_error)
        self.app.register_blueprint(bp, name='500')

        response = self.client.get('/500')

        self.assertEqual(response.status_code, 500)
        self.assertTrue("500 Internal Server Error" in str(response.data))


if __name__ == '__main__':
    unittest.main()
