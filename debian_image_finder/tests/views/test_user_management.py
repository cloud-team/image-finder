import unittest
from flask import url_for
from debian_image_finder.tests.views.base import TestBase


class TestUserManagementView(TestBase):

    def test_user_management_view(self):
        """
        Test that user management page is inaccessible without login
        and redirects to index page.
        """
        response = self.client.get(
            url_for('.user_management')
        )
        redirect_url = url_for('.index')

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, redirect_url)


if __name__ == '__main__':
    unittest.main()
