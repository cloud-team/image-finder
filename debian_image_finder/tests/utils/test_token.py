import jwt
import unittest
from datetime import datetime, timedelta
from debian_image_finder.tests.utils.base import TestBase
from debian_image_finder.utils.token import generate_payload, encode_jwt, decode_jwt


class TestTokenUtil(TestBase):
    """Tests for the Token utils."""

    def test_valid_generate_payload(self):
        payload = generate_payload('dcif')
        exp = payload['exp']
        iat = payload['iat']
        expires_at = iat + timedelta(minutes=30)

        self.assertEqual(payload['iss'], 'dcif')
        self.assertEqual(exp, expires_at)

    def test_valid_minutes_generate_payload(self):
        payload = generate_payload('dcif', minutes=60)
        exp = payload['exp']
        iat = payload['iat']
        expires_at = iat + timedelta(minutes=60)

        self.assertEqual(payload['iss'], 'dcif')
        self.assertEqual(exp, expires_at)

    def test_valid_days_generate_payload(self):
        payload = generate_payload('dcif', minutes=0, days=10)
        exp = payload['exp']
        iat = payload['iat']
        expires_at = iat + timedelta(days=10)

        self.assertEqual(payload['iss'], 'dcif')
        self.assertEqual(exp, expires_at)

    def test_valid_generate_payload_extra_data(self):
        payload = generate_payload('dcif', extra_data={'test': 'test'})
        exp = payload['exp']
        iat = payload['iat']
        expires_at = iat + timedelta(minutes=30)

        self.assertEqual(payload['iss'], 'dcif')
        self.assertEqual(payload['extra_data']['test'], 'test')
        self.assertEqual(exp, expires_at)

    def test_encode_jwt(self):
        jwt_value = encode_jwt(self.payload)
        jwt_split = jwt_value.split(".")
        decoded_jwt = decode_jwt(jwt_value)
        exp = datetime.fromtimestamp(decoded_jwt['exp'])
        iat = datetime.fromtimestamp(decoded_jwt['iat'])
        expires_at = iat + timedelta(minutes=1)
        extra_data = decoded_jwt['extra_data']

        self.assertEqual(len(jwt_split), 3)
        self.assertEqual(decoded_jwt['iss'], 'debian-cloud-image-finder')
        self.assertEqual(exp, expires_at)
        self.assertEqual(extra_data['user_public_id'], self.admin_user.public_id)
        self.assertEqual(extra_data['token_public_id'], self.service_token.public_id)

    def test_decode_jwt_valid_token(self):
        decoded_jwt = decode_jwt(self.token)
        jwt_split = self.token.split(".")

        exp = datetime.fromtimestamp(decoded_jwt['exp'])
        iat = datetime.fromtimestamp(decoded_jwt['iat'])
        expires_at = iat + timedelta(minutes=1)
        extra_data = decoded_jwt['extra_data']

        self.assertEqual(len(jwt_split), 3)
        self.assertEqual(decoded_jwt['iss'], 'debian-cloud-image-finder')
        self.assertEqual(exp, expires_at)
        self.assertEqual(extra_data['user_public_id'], self.admin_user.public_id)
        self.assertEqual(extra_data['token_public_id'], self.service_token.public_id)

    def test_decode_jwt_invalid_token(self):
        with self.assertRaises(jwt.InvalidTokenError):
            decode_jwt(self.token + '.none')

        with self.assertRaises(jwt.exceptions.InvalidSignatureError):
            decode_jwt(self.token + 'none')


if __name__ == '__main__':
    unittest.main()
