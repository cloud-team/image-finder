from flask import redirect, url_for, flash
from flask_login import login_required
from debian_image_finder.models.user import User
from debian_image_finder.utils.admin import admin_required


@login_required
@admin_required
def demote_user(public_id):

    user = User.find_by_public_id(public_id)
    if user:
        user.admin = False
        user.save_to_db()
        flash('You have successfully demoted an user.', category='success')

        return redirect(url_for('.user_management'))

    return redirect(url_for('.user_management'))
