from flask import Blueprint
from .index import index
from .provider import provider
from .edit_provider import edit_provider
from .feed import feed
from .image import image
from .login import login
from .logout import logout
from .profile import profile
from .user_management import user_management
from .promote_user import promote_user
from .demote_user import demote_user
from .service_tokens import service_tokens
from .delete_service_token import delete_service_token

bp = Blueprint('web', __name__, template_folder='templates')

bp.add_url_rule('/', view_func=index)
bp.add_url_rule('/provider/<string:vendor>', view_func=provider, methods=['GET', 'POST'])
bp.add_url_rule('/provider/<string:vendor>/feed', view_func=feed)
bp.add_url_rule('/provider/<string:vendor>/edit', view_func=edit_provider, methods=['GET', 'POST'])
bp.add_url_rule('/image/<string:uid>', view_func=image, methods=['GET', 'POST'])
bp.add_url_rule('/login', view_func=login, methods=['GET', 'POST', 'PUT', 'DELETE'])
bp.add_url_rule('/logout', view_func=logout, methods=['GET'])
bp.add_url_rule('/profile', view_func=profile, methods=['GET'])
bp.add_url_rule('/user_management', view_func=user_management, methods=['GET', 'POST'])
bp.add_url_rule('/user_management/promote/<string:public_id>', view_func=promote_user, methods=['GET'])
bp.add_url_rule('/user_management/demote/<string:public_id>', view_func=demote_user, methods=['GET'])
bp.add_url_rule('/service_tokens', view_func=service_tokens, methods=['GET', 'POST'])
bp.add_url_rule('/service_tokens/delete/<string:token_public_id>', view_func=delete_service_token, methods=['GET', 'POST'])


def init_app(app):
    app.register_blueprint(bp)
