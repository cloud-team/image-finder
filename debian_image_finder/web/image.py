from flask import render_template, request
from debian_image_finder.models.image import Image
from debian_image_finder.models.package import Package
from debian_image_finder.web.forms.package_search_form import PackageSearchForm


def image(uid):
    page = request.args.get('page', default=1, type=int)
    form = PackageSearchForm()
    image = Image.find_by_uid(uid)
    query = image.packages

    package_name = request.args.get(
        'package_name', default='', type=str
    )
    packages_per_page = request.args.get(
        'packages_per_page', default=5, type=int
    )
    packages_select_index = [5, 10, 25, 50, 100, 150, 300, 500]

    if form.validate_on_submit():
        package_name = form.package_name.data

    if package_name:
        form.package_name.data = package_name
        query = query.filter(Package.name.contains(package_name))

    packages = query.paginate(page=page, per_page=packages_per_page)

    return render_template('image.html',
                           form=form,
                           image=image,
                           provider=image.provider,
                           packages=packages,
                           package_name=package_name,
                           packages_select_index=packages_select_index)
