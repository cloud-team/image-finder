from flask import render_template
from flask_login import login_required
from debian_image_finder.models.user import User
from debian_image_finder.utils.admin import admin_required


@login_required
@admin_required
def user_management():
    users_requested_access = User.query.filter_by(admin=False).all()
    users_list = User.query.filter_by(admin=True).all()

    return render_template(
        'user_management.html',
        users_requested_access=users_requested_access,
        users_list=users_list
    )
