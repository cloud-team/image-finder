from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, SubmitField
from debian_image_finder.models.image import Image


class ProviderSearchForm(FlaskForm):
    ref = StringField('Ref')
    version = StringField('Version')
    release = SelectField('Release', choices=[('', 'Release')], default=1)
    image_type = SelectField('Type', choices=[('release', 'Type')], default=1)
    arch = SelectField('Arch', choices=[('', 'Arch')], default=1)
    region = SelectField('Region', choices=[('', 'Region')], default=1)
    submit = SubmitField('Search')

    def possibleChoices(self, provider, category):
        choices = Image.query.filter_by(provider_id=provider.id)\
            .with_entities(getattr(Image, category))\
            .order_by(getattr(Image, category)).distinct()

        for option in choices:
            getattr(self, category).choices.append((option[0], option[0]))
