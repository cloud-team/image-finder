import os
import markdown2
from flask import render_template, request, url_for
from debian_image_finder.models.provider import Provider
from debian_image_finder.models.image import Image
from debian_image_finder.web.forms.provider_search_form import (
    ProviderSearchForm
)


RESULTS_PER_PAGE = os.getenv('RESULTS_PER_PAGE', 10)


def provider(vendor):
    form = ProviderSearchForm()
    provider = Provider.find_by_vendor(vendor)

    content = markdown2.markdown(provider.markdown)
    query = Image.query.filter_by(provider_id=provider.id)
    total_count = query.count()

    page = request.args.get('page', default=1, type=int)
    ref = request.args.get('ref', default='', type=str)
    version = request.args.get('version', default='', type=str)
    vendor = request.args.get('vendor', default='', type=str)
    release = request.args.get('release', default='', type=str)
    image_type = request.args.get('image_type', default='release', type=str)
    arch = request.args.get('arch', default='', type=str)
    region = request.args.get('region', default='', type=str)

    fields = ['release', 'image_type', 'arch', 'region']
    for category in fields:
        form.possibleChoices(provider, category)

    if form.validate_on_submit():
        choices = ['ref', 'version', 'release', 'image_type', 'arch', 'region']

        for filter in choices:
            if not getattr(form, filter).data:
                continue
            query = query.filter(
                getattr(Image, filter).contains(
                    getattr(form, filter).data
                )
            )

    query = query.order_by(Image.created_at.desc())

    filtered_count = query.count()
    images = query.paginate(page=page,
                            per_page=RESULTS_PER_PAGE,
                            error_out=False)

    def url_for_page(page):
        return url_for('.provider',
                       page=page,
                       ref=ref,
                       vendor=provider.vendor,
                       version=version,
                       release=release,
                       image_type=image_type,
                       arch=arch,
                       region=region)

    return render_template('provider.html',
                           provider=provider,
                           images=images,
                           content=content,
                           form=form,
                           url_for_page=url_for_page,
                           total_count=total_count,
                           filtered_count=filtered_count)
