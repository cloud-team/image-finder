import logging
from dynaconf import settings
from datetime import datetime
from debian_image_finder.models.image import Image
from debian_image_finder.models.provider import Provider
from debian_image_finder.models.package import Package


def build_artifact_url(ref, region=None, provider_url=None):
    ami_ref = ref.split('/')[-1]
    url = ''
    if ami_ref.startswith('ami'):
        base_url = settings.BASE_AWS_ARTIFACT_URL
        url = f'{base_url}?region={region}#launchAmi={ami_ref}'
    else:
        path_url = settings.PATH_ARTIFACT_URL
        url = f'http://{provider_url}/{path_url}/{ref}'
    return url


def create_packages(packages_obj):
    packages = []
    for package_obj in packages_obj:
        db_package = Package.find_by_name_and_version(
            package_obj['name'],
            package_obj['version']
        )

        if db_package:
            packages.append(db_package)
        else:
            new_package = Package(
                name=package_obj['name'],
                version=package_obj['version'],
                tracker='https://tracker.debian.org/pkg/%s' % (
                    package_obj['name']
                )
            )
            new_package.save_to_db()
            packages.append(new_package)

    return packages


def create_provider(vendor):
    provider = Provider.find_by_vendor(vendor)
    if not provider:
        provider = Provider(
            name=vendor,
            vendor=vendor,
            description='',
            markdown=''
        )
        provider.save_to_db()

    return provider


def publish_images(data=None):
    items = data['items']

    packages = []
    images = []
    for item in items:
        if 'packages' in item['data']:
            packages_obj = item['data']['packages']
            packages = create_packages(packages_obj)

        if item['kind'] == 'Upload':
            metadata = item['metadata']
            labels = metadata['labels']
            image_type = labels['upload.cloud.debian.org/type']
            if image_type == 'release' or image_type == 'daily':
                uid = metadata['uid']
                arch = labels['debian.org/arch']
                release = labels['debian.org/release']
                vendor = labels['cloud.debian.org/vendor']
                version = labels['cloud.debian.org/version']

                provider_url = item['data']['provider']
                ref = item['data']['ref']

                if 'aws.amazon.com/region' in labels:
                    region = labels['aws.amazon.com/region']
                else:
                    region = ''

                try:
                    artifact_url = build_artifact_url(
                        ref=ref,
                        region=region,
                        provider_url=provider_url
                    )
                except Exception as exception:
                    logging.error("Error when building artifact url.")
                    logging.error(exception)

                try:
                    provider = create_provider(vendor)
                except Exception as exception:
                    logging.error("Error when finding or creating provider.")
                    logging.error(exception)

                image = Image.find_by_uid(uid)
                if image:
                    logging.warning('Image already exists.')
                    continue

                image = Image(
                    uid=uid,
                    arch=arch,
                    release=release,
                    image_type=image_type,
                    vendor=vendor,
                    version=version,
                    region=region,
                    artifact_url=artifact_url,
                    ref=ref,
                    provider_id=provider.id
                )

                created_at = None

                try:
                    created_at = datetime.strptime(
                        version.split('-')[0], '%Y%m%d'
                    )
                    image.created_at = created_at
                except Exception as exception:
                    logging.info("Could not format version to datetime.")
                    logging.info(exception)

                for package in packages:
                    image.packages.append(package)

                try:
                    image.save_to_db()
                    images.append(image)
                except Exception as exception:
                    logging.error("Error when commiting image to database.")
                    logging.error(exception)

    return images
