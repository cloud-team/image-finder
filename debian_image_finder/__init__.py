from flask import Flask
from debian_image_finder.extensions import configuration

__version__ = '0.2.2'


def create_app():
    app = Flask(__name__)
    app.app_context().push()
    configuration.init_app(app)
    configuration.load_extensions(app)

    return app
