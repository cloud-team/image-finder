from flasgger import swag_from
from flask_restful import Resource
from debian_image_finder.models.provider import Provider
from debian_image_finder.schemas.provider import providers_schema


class ProvidersAPI(Resource):

    @swag_from('docs/providers/get.yml')
    def get(self):
        providers = Provider.find_all()
        return providers_schema.dump(providers), 200
