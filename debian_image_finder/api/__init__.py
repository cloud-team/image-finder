from flask import Blueprint
from flask_restful import Api
from .user import UserAPI
from .users import UsersAPI
from .promote_user import PromoteUserAPI
from .demote_user import DemoteUserAPI
from .provider import ProviderAPI
from .providers import ProvidersAPI
from .image import ImageAPI
from .images import ImagesAPI
from .publish import PublishAPI


bp = Blueprint('restapi', __name__, url_prefix='/api/v1')
api = Api(bp)

# User
api.add_resource(UsersAPI, '/users', endpoint='users', methods=['GET'])
api.add_resource(UserAPI, '/user', endpoint="create_user", methods=['POST'])
api.add_resource(UserAPI, '/user/<string:public_id>', endpoint="user", methods=['GET', 'PUT', 'DELETE'])

# Promote User
api.add_resource(PromoteUserAPI, '/user/promote/<string:public_id>', methods=['PUT'])

# Demote User
api.add_resource(DemoteUserAPI, '/user/demote/<string:public_id>', methods=['PUT'])

# Provider
api.add_resource(ProvidersAPI, '/providers', endpoint='providers', methods=['GET'])
api.add_resource(ProviderAPI, '/provider', endpoint='create_provider', methods=['POST'])
api.add_resource(ProviderAPI, '/provider/<string:vendor>', endpoint="provider", methods=['GET', 'PUT', 'DELETE'])

# Image
api.add_resource(ImagesAPI, '/images', endpoint='images', methods=['GET'])
api.add_resource(ImageAPI, '/image', endpoint='create_image', methods=['POST'])
api.add_resource(ImageAPI, '/image/<string:uid>', endpoint="image", methods=['GET', 'PUT', 'DELETE'])

# Publish
api.add_resource(PublishAPI, '/publish', endpoint='publish', methods=['POST'])


def init_app(app):
    app.register_blueprint(bp)
