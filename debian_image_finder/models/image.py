from datetime import datetime
from typing import List
from debian_image_finder.extensions.database import db
from debian_image_finder.models.images_packages import images_packages


class Image(db.Model):
    __tablename__ = "images"
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    uid = db.Column(db.String(255), unique=True)
    arch = db.Column(db.String(255), nullable=False)
    release = db.Column(db.String(255), nullable=False)
    image_type = db.Column(db.String(255), nullable=False)
    vendor = db.Column(db.String(255), nullable=False)
    version = db.Column(db.String(255), nullable=False)
    region = db.Column(db.String(255))
    artifact_url = db.Column(db.String(255))
    ref = db.Column(db.String(255))
    created_at = db.Column(db.DateTime(), nullable=False)
    provider_id = db.Column(db.Integer, db.ForeignKey('providers.id'))

    provider = db.relationship('Provider', foreign_keys=provider_id)

    packages = db.relationship('Package',
                               secondary=images_packages,
                               lazy='dynamic',
                               backref=db.backref('images', lazy='dynamic'))

    def __init__(self,
                 uid,
                 arch,
                 release,
                 image_type,
                 vendor,
                 version,
                 region,
                 artifact_url,
                 ref,
                 provider_id):
        self.uid = uid
        self.arch = arch
        self.release = release
        self.image_type = image_type
        self.vendor = vendor
        self.version = version
        self.region = region
        self.artifact_url = artifact_url
        self.ref = ref
        self.created_at = datetime.utcnow()
        self.provider_id = provider_id

    @classmethod
    def find_by_id(cls, _id) -> 'Image':
        return cls.query.filter_by(id=_id).first_or_404()

    @classmethod
    def find_by_uid(cls, uid) -> 'Image':
        return cls.query.filter_by(uid=uid).first()

    @classmethod
    def find_by_ref(cls, ref) -> 'Image':
        return cls.query.filter_by(ref=ref).first_or_404()

    @classmethod
    def find_by_provider(cls, provider) -> 'Image':
        return cls.query.filter_by(provider=provider)

    @classmethod
    def find_all(cls) -> List['Image']:
        return cls.query.all()

    def update(self, data) -> 'Image':
        self.arch = data['arch']
        self.release = data['release']
        self.image_type = data['image_type']
        self.vendor = data['vendor']
        self.version = data['version']
        self.region = data['region']
        self.ref = data['ref']
        self.artifact_url = data['artifact_url']
        return self

    def save_to_db(self) -> None:
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self) -> None:
        db.session.delete(self)
        db.session.commit()

    def __repr__(self):
        return 'Image(ref=%s, arch=%s, version=%s, vendor=%s)' % (
            self.ref, self.arch, self.version, self.vendor)
