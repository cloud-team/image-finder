import datetime
from typing import List
import uuid
from flask import redirect, url_for
from flask_login import UserMixin
from debian_image_finder.extensions.database import db
from debian_image_finder.extensions.login import login_manager


@login_manager.user_loader
def get_user(user_id):
    return User.find_by_id(user_id)


@login_manager.unauthorized_handler
def unauthorized_callback():
    return redirect(url_for('.index'))


class User(db.Model, UserMixin):
    __tablename__ = "users"

    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    public_id = db.Column(db.String(255), unique=True)
    name = db.Column(db.String(86))
    email = db.Column(db.String(86), unique=True)
    username = db.Column(db.String(86), unique=True, nullable=False)
    avatar_url = db.Column(db.String(255))
    admin = db.Column(db.Boolean, nullable=False)
    created_at = db.Column(db.DateTime(), nullable=False)

    def __init__(self, name, email, username, avatar_url, admin):
        self.public_id = str(uuid.uuid4())
        self.name = name
        self.email = email
        self.username = username
        self.avatar_url = avatar_url
        self.admin = admin
        self.created_at = datetime.datetime.utcnow()

    @classmethod
    def find_by_id(cls, _id) -> 'User':
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def find_by_public_id(cls, public_id) -> 'User':
        return cls.query.filter_by(public_id=public_id).first()

    @classmethod
    def find_by_name(cls, name) -> 'User':
        return cls.query.filter_by(name=name).first()

    @classmethod
    def find_by_username(cls, username) -> 'User':
        return cls.query.filter_by(username=username).first()

    @classmethod
    def find_all(cls) -> List['User']:
        return cls.query.all()

    def save_to_db(self) -> None:
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self) -> None:
        db.session.delete(self)
        db.session.commit()

    def __repr__(self):
        return 'User(public_id=%s, username=%s, admin=%s)' % (
            self.public_id, self.username, self.admin)
