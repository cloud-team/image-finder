import datetime
from typing import List
from debian_image_finder.extensions.database import db


class Provider(db.Model):
    __tablename__ = 'providers'

    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    vendor = db.Column(db.String(80), unique=True, nullable=False)
    description = db.Column(db.Text)
    markdown = db.Column(db.Text, nullable=True)
    created_at = db.Column(db.DateTime(), nullable=False)

    def __init__(self, name, vendor, description, markdown):
        self.name = name
        self.vendor = vendor
        self.description = description
        self.markdown = markdown
        self.created_at = datetime.datetime.utcnow()

    @classmethod
    def find_by_id(cls, _id) -> 'Provider':
        return cls.query.filter_by(id=_id).first_or_404()

    @classmethod
    def find_by_name(cls, name) -> 'Provider':
        return cls.query.filter_by(name=name).first()

    @classmethod
    def find_by_vendor(cls, vendor) -> 'Provider':
        return cls.query.filter_by(vendor=vendor).first()

    @classmethod
    def find_all(cls) -> List['Provider']:
        return cls.query.all()

    def update(self, data) -> 'Provider':
        self.name = data['name']
        self.vendor = data['vendor']
        self.description = data['description']
        self.markdown = data['markdown']
        return self

    def save_to_db(self) -> None:
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self) -> None:
        db.session.delete(self)
        db.session.commit()

    def __repr__(self):
        return 'Provider(name=%s, vendor=%s, description=%s, markdown=%s)' % (
            self.name, self.vendor, self.description, self.markdown)
