#!/bin/sh

set -e

flask db upgrade

if [ "$ENV_FOR_DYNACONF" = "production" ]; then
    gunicorn -c gunicorn.config.py "debian_image_finder:create_app()"
else
    flask run --host 0.0.0.0
fi
