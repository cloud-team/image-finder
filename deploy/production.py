import sys
from utils import get_file_from_repository
from utils import convert_file_content_base64_to_yaml
from utils import convert_content_to_base64
from utils import commit_file
from utils import ARGOCD_VALUES, CI_COMMIT_TAG, GITOPS_BRANCH, PROJECT_ID


if __name__ == "__main__":
    file = get_file_from_repository(PROJECT_ID, ARGOCD_VALUES, GITOPS_BRANCH)
    obj = convert_file_content_base64_to_yaml(file)

    if obj['production']['image']['tag'] == CI_COMMIT_TAG:
        print(f'{CI_COMMIT_TAG} is already the latest for the production environment.')  # noqa: E501
        sys.exit()

    obj['production']['image']['tag'] = CI_COMMIT_TAG
    content = convert_content_to_base64(obj)

    commit_file(
        PROJECT_ID,
        'update',
        ARGOCD_VALUES,
        GITOPS_BRANCH,
        f'Update production image tag to {CI_COMMIT_TAG} inside values.yaml',
        content
    )
