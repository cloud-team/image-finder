import os
import json
import yaml
import base64
import requests
from urllib.parse import quote

GITLAB_TOKEN = os.environ['GITLAB_TOKEN']
GITLAB_HOST = os.environ.get('GITLAB_HOST', 'https://salsa.debian.org')
CI_COMMIT_TAG = os.environ.get('CI_COMMIT_TAG', '')
CI_COMMIT_SHORT_SHA = os.environ.get('CI_COMMIT_SHORT_SHA', '')
PROJECT_ID = 35743  # https://salsa.debian.org/cloud-team/image-finder
ARGOCD_VALUES = 'argocd/values.yaml'
GITOPS_BRANCH = 'infrastructure'


headers = {
    'Authorization': f'Bearer {GITLAB_TOKEN}',
    'Content-Type': 'application/json'
}


def get_file_from_repository(project_id, file_path, ref):
    obj = {}

    file_path = quote(file_path, safe='')

    response = requests.get(
        f'{GITLAB_HOST}/api/v4/projects/{project_id}/repository/files/{file_path}?ref={ref}',  # noqa: E501
        headers=headers
    )

    if response.status_code == requests.codes.ok:
        obj = json.loads(response.text)

    return obj


def commit_file(project_id, action, file_path,
                branch, commit_message, content):

    obj = {}

    data = {
        'branch': branch,
        'commit_message': commit_message,
        'actions': [
            {
                'action': action,
                'file_path': file_path,
                'content': content,
                'encoding': 'base64'
            }
        ]
    }

    response = requests.post(
        f'{GITLAB_HOST}/api/v4/projects/{project_id}/repository/commits',
        headers=headers,
        data=json.dumps(data)
    )

    if response.status_code == requests.codes.ok:
        obj = json.loads(response.text)

    return obj


def convert_file_content_base64_to_yaml(file):
    content = base64.b64decode(file['content'])
    try:
        yaml_content_obj = yaml.safe_load(content)
    except yaml.YAMLError as exc:
        print(exc)

    return yaml_content_obj


def convert_content_to_base64(yaml_content_obj):
    try:
        yaml_content_obj = yaml.dump(yaml_content_obj).encode()
    except yaml.YAMLError as exc:
        print(exc)

    content = base64.b64encode(yaml_content_obj).decode()

    return content
