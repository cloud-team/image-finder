DOCKER_BUILD=docker build --network host -t debian-image-finder .
DOCKER_RUN=docker run -it --rm \
		--env-file .env \
		--entrypoint="" \
		-v $(PWD):/debian-image-finder \
		-p 5000:5000 \
		debian-image-finder \

build:
	$(DOCKER_BUILD)

run: build
	docker run -it --rm \
		--env-file .env \
		-v $(PWD):/debian-image-finder \
		-p 5000:5000 \
		debian-image-finder

shell: build
	$(DOCKER_RUN) bash

test:
	$(DOCKER_RUN) ./bin/quick-setup.sh test

test-module:
	bash test-module.sh $(module-name)

lint:
	$(DOCKER_RUN) ./bin/quick-setup.sh lint

lint-chart:
	docker run --rm \
		--env-file .env \
		--entrypoint="" \
		--workdir=/debian-image-finder \
		-v $(PWD):/debian-image-finder \
		dtzar/helm-kubectl \
		./bin/quick-setup.sh lint-chart

deploy-staging:
	$(DOCKER_RUN) ./bin/quick-setup.sh deploy_staging

deploy-production:
	$(DOCKER_RUN) ./bin/quick-setup.sh deploy_production

coverage:
	$(DOCKER_RUN) bin/quick-setup.sh coverage

migrate-db:
	$(DOCKER_RUN) flask db migrate

upgrade-db:
	$(DOCKER_RUN) flask db upgrade

create-db:
	$(DOCKER_RUN) flask create-db

drop-db:
	$(DOCKER_RUN) flask drop-db

recreate-db:
	$(DOCKER_RUN) flask recreate-db

delete-images:
	$(DOCKER_RUN) flask delete-images

delete-users:
	$(DOCKER_RUN) flask delete-users

promote-user:
	$(DOCKER_RUN) flask promote-user

demote-user:
	$(DOCKER_RUN) flask unpromote-user

seed:
	$(DOCKER_RUN) flask seed run
