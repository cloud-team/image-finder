FROM debian:unstable-slim

LABEL Cloud Team "debian-cloud@lists.debian.org"

EXPOSE 5000

WORKDIR /debian-image-finder

ARG NON_INTERACTIVE="1"
ARG DOCKER_ENVIRONMENT="1"
ARG DEBIAN_FRONTEND=noninteractive
ARG DEBCONF_NOWARNINGS="yes"

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY bin/quick-setup.sh /debian-image-finder/bin/quick-setup.sh

RUN ./bin/quick-setup.sh install_packages

COPY . /debian-image-finder

ENTRYPOINT ["./docker-entrypoint.sh"]
